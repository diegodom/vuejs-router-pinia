import { ref } from 'vue';
import axios from 'axios';

export const usePokemons = () => {
  const loading = ref(true);
  const data = ref(null);

  const getPokemons = async (name = '') => {
    loading.value = true;
    try {
      const res = await axios.get(`https://pokeapi.co/api/v2/pokemon/${name}`);
      data.value = res.data;
    } catch (error) {
      console.log(error);
      data.value = null;
    } finally {
      loading.value = false;
    }
  };

  return {
    getPokemons,
    loading,
    data,
  };
};
